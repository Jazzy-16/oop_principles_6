package oop_principles.abstraction;

public class IPhone extends Phone implements WiFi, Camera, Bluetooth{

    @Override
    public void call() {
        System.out.println("IPhone calls");
    }

    @Override
    public void text() {
        System.out.println("IPhone texts");
    }

    @Override
    public void connectWiFi() {
        System.out.println("iPhone connects WiFi");

    }

    @Override
    public void ring(){
        System.out.println("IPhone rings");


    }

    @Override
    public void takePhoto() {
        System.out.println("iPhone takes a photo");
    }

    @Override
    public void recordVideo() {
        System.out.println("iPhone records video");

    }

    @Override
    public void connectBluetooth() {
        System.out.println("iPhone connects to Bluetooth");
    }
}