package oop_principles.abstraction;

public interface WiFi {
    // We can only have public static final instance variables in an interface
    public static final String variable = "";
   // Phone phone = new Phone();
    void connectWiFi();//by default, it's public and abstract

    default void getWiFi(){
        //body
    }
    static void method() {

    }
}
