package class_vs_object;

public class Car {

    // default constructor - programmer can create an object with no info
    public String make;
    public String model;
    public int year;
    public double price;

    // create 4-args constructor
    public Car(String make, String model, int year, double price) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.price = price;
    }

    // override toString method
    @Override
    public String toString() {
        return "Car{" +
                "make='" + make + '\'' +
                ", model='" + model + '\'' +
                ", year=" + year +
                ", price=" + price +
                '}';
    }

}
