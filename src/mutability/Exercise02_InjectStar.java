package mutability;

public class Exercise02_InjectStar {

        /*
    Create a method that takes a String and returns a String
    if the given String have even length and length is at least 2, then insert * in the middle of the String
    aa -> a*a
    java -> ja*va
    toyota -> toy*ota


    if the given String has odd length and length is at least 3, then insert * in before and after the middle character
    aaa -> a*a*a
    hello -> he*l*lo


    ""      -> ""
    "a"     -> ""
    "aa"    -> "a*a"
    "aaa"   -> "a*a*a"


     */

    //one way
  /*  public static String injectStart(String str) {
        StringBuilder sb = new StringBuilder(str);
        if (str.length() % 2 == 0 && str.length() >= 2) {
            sb.insert(str.length() / 2, "*");
        } else if (str.length() % 2 == 1 && str.length() >= 3) {
            sb.insert(str.length() / 2, "*");
            sb.insert(str.length() / 2 + 2, "*");
        }
        return sb.toString();
    }
    */

    //Second way

    public static String injectStar(String str){
        if(str.length() <= 1) return "";
        else if(str.length() % 2 == 0) return  str.substring(0, str.length()/2) + "*" + str.substring(str.length()/2);
        return str.substring(0, str.length()/2) + "*" + str.charAt(str.length()/2) + "*" + str.substring(str.length()/2+1);
    }
}
